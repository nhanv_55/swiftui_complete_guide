import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack() {
            Image("trang-an")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .cornerRadius(8)
                .padding(.horizontal, 15)
            
            Text("First Line")
                .font(.largeTitle)
                .foregroundColor(.blue)
            Text("Second Line")
                .font(.title)
                .foregroundColor(.orange)
            
            HStack {
                Text("Left Side")
                Text("Right Side")
                    .padding(.all, 10)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
