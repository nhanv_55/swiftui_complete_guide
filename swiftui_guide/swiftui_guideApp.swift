//
//  swiftui_guideApp.swift
//  swiftui_guide
//
//  Created by NhaNV on 23/07/2021.
//

import SwiftUI

@main
struct swiftui_guideApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
